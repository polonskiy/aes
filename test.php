<?php

require __DIR__.'/AES.php';

$key = uniqid();

$data = str_repeat(chr(mt_rand(1, 1000)), mt_rand(1, 1000));
assert(($a = AES::decrypt(AES::encrypt($data, $key), $key)) === $data);

$data = str_repeat('x ', mt_rand(1, 1000));
assert(AES::decrypt(AES::encrypt($data, $key), $key) === $data);

$data = str_repeat(' ', mt_rand(1, 1000));
assert(AES::decrypt(AES::encrypt($data, $key), $key) === $data);
