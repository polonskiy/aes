<?php

class AES {

	public static function encrypt($plaintext, $key) {
		$key = hash('sha256', $key, true);
		$ivsize = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
		$iv = mcrypt_create_iv($ivsize, MCRYPT_DEV_URANDOM);
		$plaintext .= '|';
		$ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $plaintext, MCRYPT_MODE_CBC, $iv);
		$ciphertext = $iv.$ciphertext;
		return $ciphertext;
	}

	public static function decrypt($ciphertext, $key) {
		$key = hash('sha256', $key, true);
		$ivsize = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
		$iv = substr($ciphertext, 0, $ivsize);
		$ciphertext = substr($ciphertext, $ivsize);
		$plaintext = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $ciphertext, MCRYPT_MODE_CBC, $iv);
		$plaintext = substr($plaintext, 0, strrpos($plaintext, '|'));
		return $plaintext;
	}

}
